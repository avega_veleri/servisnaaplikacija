/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author synan
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            AnnotationConfiguration config = new AnnotationConfiguration().configure();
            config.addAnnotatedClass(Grad.class);
            config.addAnnotatedClass(Serviser.class);
            config.addAnnotatedClass(Klijent.class);
            config.addAnnotatedClass(TipObjekta.class);
            config.addAnnotatedClass(Objekt.class);
            config.addAnnotatedClass(Reklamacija.class);
            sessionFactory = config.buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
