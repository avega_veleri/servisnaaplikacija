/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import java.io.Serializable;

import javax.persistence.*;           
    
    
    
    
@Entity
@Table (name="serviser")
public class Serviser implements Serializable{
    
    @Id
    @Column (name="OIB")
    private String oib;
    @Column (name="ImePrezime")
    private String ImePrezime;
    @Column (name="SluzbeniTelefon")
    private String Sluzbenitelefon;

    public Serviser() {
    }
    
    
    public Serviser(String ImePrez,String SluzTel,String oib){
        this.ImePrezime = ImePrez;
        this.Sluzbenitelefon = SluzTel;
        this.oib = oib;
    }


    public String getImePrezime() {
        return ImePrezime;
    }

    public String getSluzbenitelefon() {
        return Sluzbenitelefon;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public void setImePrezime(String ImePrezime) {
        this.ImePrezime = ImePrezime;
    }

    public void setSluzbenitelefon(String Sluzbenitelefon) {
        this.Sluzbenitelefon = Sluzbenitelefon;
    }

    
        
}

