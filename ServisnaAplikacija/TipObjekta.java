/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table (name="tip_objekta")
public class TipObjekta implements Serializable{
    
    @Id
    private String Sifra;
    private String Naziv;

    public TipObjekta() {
    }
    
    
    public TipObjekta(String sifra,String naziv)
    {
        
    this.Sifra = sifra;
    this.Naziv = naziv;
    
    }

    public String getNaziv() {
        return Naziv;
    }

    public String getSifra() {
        return Sifra;
    }

    public void setSifra(String Sifra) {
        this.Sifra = Sifra;
    }

    public void setNaziv(String Naziv) {
        this.Naziv = Naziv;
    }
    
}
