/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="objekt")
public class Objekt implements Serializable{
    
    @Id
    @Column(name="Adresa")
    private String adresa;
    @Column (name="Naziv")
    private String naziv;
    
    @ManyToOne
    @JoinColumn (name="Grad")
    private Grad grad;
    
    @ManyToOne
    @JoinColumn (name="Vlasnik")
    private Klijent vlasnik;
    
    @ManyToOne
    @JoinColumn (name="Tip")
    private TipObjekta tip;

    public Objekt() {
    }
    
    public Objekt(String naziv,String adresa,Grad grad,Klijent vlasnik,TipObjekta sifra){
        
        this.naziv=naziv;
        this.adresa=adresa;
        this.grad =grad;
        this.vlasnik = vlasnik;
        this.tip = sifra;
    }

    public String getAdresa() {
        return adresa;
    }

    public Grad getGrad() {
        return grad;
    }

    public String getNaziv() {
        return naziv;
    }

    public TipObjekta getTip() {
        return tip;
    }

    public Klijent getVlasnik() {
        return vlasnik;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setGrad(Grad grad) {
        this.grad = grad;
    }

    public void setVlasnik(Klijent vlasnik) {
        this.vlasnik = vlasnik;
    }

    public void setTip(TipObjekta tip) {
        this.tip = tip;
    }
    
}

