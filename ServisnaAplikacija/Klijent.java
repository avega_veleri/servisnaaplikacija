/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name="klijent")
public class Klijent implements Serializable{
    
    @Id
    @Column(name="OIB")
    private String oib;
    @Column(name="Naziv")
    private String naziv;
    @Column(name="Adresa")
    private String adresa;
    
    @ManyToOne
    @JoinColumn(name="Grad")
    private Grad grad;
    @Column(name="Telefon")
    private String telefon;
    
    public Klijent(){
    
    }
    
    public Klijent(String naziv, String adresa, Grad grad, String telefon, String oib) {
        this.naziv = naziv;
        this.adresa = adresa;
        this.grad = grad;
        this.telefon = telefon;
        this.oib = oib;
    }

    
   
    
    public String getAdresa() {
        return adresa;
    }

    public Grad getGrad() {
        return grad;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getOib() {
        return oib;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setGrad(Grad grad) {
        this.grad = grad;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }
    
    
}
