/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;


@Entity
@Table (name="reklamacija")
public class Reklamacija implements Serializable{

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column (name="brRek")
    private int BrojReklamacije;

    @Temporal (TemporalType.DATE)
    @Column (name="Datum")
    private Calendar DatumReklamacije;

    @ManyToOne
    @JoinColumn (name="Objekt")
    private Objekt objekt;

    @Column (name="Kvar")
    private String Kvar;

    @ManyToOne
    @JoinColumn (name="Serviser")
    private Serviser serviser;

    @ManyToOne
    @JoinColumn (name="Vlasnik")
    private Klijent klijent;

    public Reklamacija() {
    }



    public Reklamacija(Calendar DatumRek, Objekt objekt,Serviser serviser, String kvar){
        this.DatumReklamacije=DatumRek;
        this.objekt=objekt;
        this.Kvar = kvar;
        this.serviser = serviser;
        this.klijent = objekt.getVlasnik();

    }

    public int getBrojReklamacije() {
        return BrojReklamacije;
    }

    public void setBrojReklamacije(int BrojReklamacije) {
        this.BrojReklamacije = BrojReklamacije;
    }

    public Calendar getDatumReklamacije() {
        return DatumReklamacije;
    }

    public void setDatumReklamacije(Calendar DatumReklamacije) {
        this.DatumReklamacije = DatumReklamacije;
    }

    public Objekt getObjekt() {
        return objekt;
    }

    public void setObjekt(Objekt objekt) {
        this.objekt = objekt;
    }

    public String getKvar() {
        return Kvar;
    }

    public void setKvar(String Kvar) {
        this.Kvar = Kvar;
    }

    public Serviser getServiser() {
        return serviser;
    }

    public void setServiser(Serviser serviser) {
        this.serviser = serviser;
    }

    public Klijent getKlijent() {
        return klijent;
    }

    public void setKlijent(Klijent klijent) {
        this.klijent = klijent;
    }



}

