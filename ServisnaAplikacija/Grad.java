/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ServisnaAplikacija;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="grad")
public class Grad implements Serializable{
    @Id
    @Column(name="pBroj")
    private int postanskiBroj;
    
    @Column(name="Naziv")
    private String naziv;
    
    public Grad(){}

    public Grad(int pBroj, String naziv){
        this.postanskiBroj = pBroj;
        this.naziv = naziv;
    }
    public String getNaziv() {
        return naziv;
    }

    public int getPostanskiBroj() {
        return postanskiBroj;
    }

    public void setPostanskiBroj(int postanskiBroj) {
        this.postanskiBroj = postanskiBroj;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
    
    
    
}
